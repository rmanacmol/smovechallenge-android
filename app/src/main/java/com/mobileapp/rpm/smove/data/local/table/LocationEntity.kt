package com.mobileapp.rpm.smove.data.local.table

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.mobileapp.rpm.smove.data.local.RoomContract

@Entity(tableName = RoomContract.table_location)
data class LocationEntity(
        @PrimaryKey(autoGenerate = true) val iid: Int,
        var latitude: String = "",
        var is_on_trip: Boolean = false,
        var id: Int = 0,
        var longitude: String = "")