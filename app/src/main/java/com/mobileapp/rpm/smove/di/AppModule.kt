package com.mobileapp.rpm.smove.di

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val smoveApplication: SmoveApplication) {

    @Provides
    @Singleton
    fun provideContext(): Context = smoveApplication

}