package com.mobileapp.rpm.smove.model

data class Location(
        var latitude: String = "",
        var is_on_trip: Boolean = false,
        var id: Int = 0,
        var longitude: String = ""
)