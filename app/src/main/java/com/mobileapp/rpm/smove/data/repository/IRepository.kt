package com.mobileapp.rpm.smove.data.repository

import android.arch.lifecycle.MutableLiveData
import com.mobileapp.rpm.smove.model.Car
import com.mobileapp.rpm.smove.model.Location

interface IRepository {

    fun getCar(): MutableLiveData<Car>

    fun addLocationToLocal(listUser: List<Location>)

    fun getLocationLocal(): MutableLiveData<List<Location>>

}