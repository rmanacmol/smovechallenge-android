package com.mobileapp.rpm.smove.data.remote

import javax.inject.Inject

class RemoteData @Inject constructor(private val remoteService: RemoteService) {

    fun netCallGetCar() = remoteService.netCallGetCar()

}