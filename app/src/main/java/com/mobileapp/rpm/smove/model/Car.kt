package com.mobileapp.rpm.smove.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class Car(@Expose @SerializedName("data") val locationList: ArrayList<Location>)