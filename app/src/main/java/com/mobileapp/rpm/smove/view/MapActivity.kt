package com.mobileapp.rpm.smove.view

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Criteria
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mobileapp.rpm.smove.R
import com.mobileapp.rpm.smove.util.LocationUtil
import com.mobileapp.rpm.smove.viewmodel.MapViewModel
import kotlinx.android.synthetic.main.activity_map.*


class MapActivity : AppCompatActivity(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null
    private var viewModel: MapViewModel? = null
    private var autocompleteFragment: PlaceAutocompleteFragment? = null
    var locationManager: LocationManager? = null

    var radius: Double = 8000.0
    var zoomLevel: Float = 11.5f
    var latti: Double = 1.352083
    var longi: Double = 103.819836

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        viewModel = ViewModelProviders.of(this).get(MapViewModel::class.java)

        askLocation()

        //init map
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as MapFragment
        mapFragment.getMapAsync(this)

        //google map search
        autocompleteFragment = fragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as PlaceAutocompleteFragment
        autocompleteFragment?.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                latti = place.latLng.latitude
                longi = place.latLng.longitude
                setupMarkers(place.latLng.latitude, place.latLng.longitude, radius, zoomLevel)

            }

            override fun onError(status: Status) {

            }
        })

        autocompleteFragment?.setHint("Search your location")

    }

    override fun onResume() {
        super.onResume()
        askLocation()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        setupMarkers(latti, longi, radius, zoomLevel)
    }

    fun setupMarkers(lat: Double, longi: Double, radiusUp: Double, zoomLevelUp: Float) {
        val latLng = LatLng(lat, longi)
        val address = LocationUtil.getAddress(this, lat, longi)
        tvCurrentLocation.text = address

        autocompleteFragment?.setText("")
        mMap?.clear()

        //Current Location Marker
        val circleoptions = CircleOptions().strokeWidth(1f).strokeColor(Color.BLUE).fillColor(Color.parseColor("#500084d3"))
        mMap?.addMarker(MarkerOptions().position(latLng).title(address))

        mMap?.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap?.addCircle(circleoptions.center(latLng).radius(radiusUp))

        //List Marker
        viewModel?.getCar()?.observe(this,
                Observer { car ->
                    if (car != null) {
                        viewModel?.addLocationoLocal(car.locationList)

                        var drawable: Int
                        var onTripCar = 0
                        var availableCar = 0

                        for (item in car.locationList) {

                            if (item.is_on_trip) {
                                drawable = R.drawable.ic_car
                                onTripCar++

                            } else {
                                drawable = R.drawable.ic_car_available
                                availableCar++
                            }

                            val latLngList = LatLng(item.latitude.toDouble(), item.longitude.toDouble())

                            mMap?.addMarker(MarkerOptions()
                                    .position(latLngList)
                                    .title(item.id.toString())
                                    .icon(BitmapDescriptorFactory.fromResource(drawable)))
                        }

                        tvCarsAvailable.text = resources.getString(R.string.car_available, availableCar.toString())
                        tvCarsOnTrip.text = resources.getString(R.string.car_on_trip, onTripCar.toString())

                    } else {
                        callDataFromLocal()
                        Toast.makeText(this, "Failed Fetching Remote Data", Toast.LENGTH_LONG).show()
                    }
                })

        mMap?.mapType = GoogleMap.MAP_TYPE_HYBRID
        mMap?.isTrafficEnabled = false
        mMap?.isIndoorEnabled = false
        mMap?.isBuildingsEnabled = false
        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(circleoptions.center, zoomLevelUp))

    }

    private fun callDataFromLocal() {
        viewModel?.getLocationFromLocal()?.observe(this, Observer { locationList ->
            if (locationList != null) {
                for (item in locationList) {
                    Log.d("MAP", "Persisted location:" + locationList)
                }
            }
        })
    }

    //Ask User Location

    protected fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes") { dialog, id -> startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)) }
                .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }

    private fun getLocation() {
        if (ActivityCompat.checkSelfPermission(this@MapActivity, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this@MapActivity, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this@MapActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)

        } else {
            val location = locationManager?.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
            val location1 = locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            val location2 = locationManager?.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER)

            if (location != null) {
                latti = location.latitude
                longi = location.longitude

            } else if (location1 != null) {
                latti = location1.latitude
                longi = location1.longitude

            } else if (location2 != null) {

                latti = location2.latitude
                longi = location2.longitude

            } else {

                val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
                val criteria = Criteria()
                val bestProvider = locationManager.getBestProvider(criteria, false)

                //in case location is null
                latti = 1.352083
                longi = 103.819836

                if (locationManager.getLastKnownLocation(bestProvider) != null) {
                    latti = locationManager.getLastKnownLocation(bestProvider).latitude
                    longi = locationManager.getLastKnownLocation(bestProvider).longitude
                }
            }
        }
    }

    private fun askLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps()
        } else if (locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation()
        }

    }
}
