package com.mobileapp.rpm.smove.util

import android.content.Context
import android.location.Geocoder
import java.io.IOException
import java.util.*

object LocationUtil {

    fun getAddress(context: Context, lat: Double, longi: Double): String {
        val geocoder = Geocoder(context, Locale.getDefault())
        var address = ""
        try {
            val addressList = geocoder.getFromLocation(lat, longi, 1)
            if (addressList != null) {
                address = addressList[0].getAddressLine(0)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return address
    }
}
