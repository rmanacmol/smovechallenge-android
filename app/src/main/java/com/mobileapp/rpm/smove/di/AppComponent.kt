package com.mobileapp.rpm.smove.di

import com.mobileapp.rpm.smove.viewmodel.BaseViewModel
import dagger.Component
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class, RoomModule::class, NetModule::class))
@Singleton
interface AppComponent {

    fun inject(baseViewModel: BaseViewModel)

}