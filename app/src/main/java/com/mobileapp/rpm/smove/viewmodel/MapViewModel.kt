package com.mobileapp.rpm.smove.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.mobileapp.rpm.smove.model.Car
import com.mobileapp.rpm.smove.model.Location
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class MapViewModel : BaseViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private var liveData: LiveData<List<Location>>? = null

    fun getCar(): LiveData<Car>? {
        return repository.getCar()
    }

    fun addLocationoLocal(location: List<Location>) {
        Completable.fromAction { repository.addLocationToLocal(location) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(@NonNull d: Disposable) {
                        compositeDisposable.add(d)
                    }

                    override fun onComplete() {
                        Timber.i("DataSource has been populated")
                    }

                    override fun onError(@NonNull e: Throwable) {
                        e.printStackTrace()
                        Timber.i("DataSource hasn't been populated")
                    }
                })
    }

    fun getLocationFromLocal(): LiveData<List<Location>>? {
        if (liveData == null) {
            liveData = MutableLiveData<List<Location>>()
            liveData = repository.getLocationLocal()
        }
        return liveData

    }

}