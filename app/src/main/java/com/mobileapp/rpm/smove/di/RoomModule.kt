package com.mobileapp.rpm.smove.di

import android.content.Context
import com.mobileapp.rpm.smove.data.local.RoomData
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {

    @Provides
    @Singleton
    fun provideRoomCurrencyDataSource(context: Context) = RoomData.buildPersistentCurrency(context)

}