package com.mobileapp.rpm.smove.data.local.table

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.mobileapp.rpm.smove.data.local.RoomContract
import io.reactivex.Flowable

@Dao
interface LocationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(locationEntity: List<LocationEntity>)

    @Query(RoomContract.select_from_location)
    fun selectAll(): Flowable<List<LocationEntity>>

}

