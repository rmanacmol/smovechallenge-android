package com.mobileapp.rpm.smove.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.mobileapp.rpm.smove.data.local.table.LocationDao
import com.mobileapp.rpm.smove.data.local.table.LocationEntity

@Database(entities = arrayOf(LocationEntity::class), version = 1)
abstract class RoomData : RoomDatabase() {

    abstract fun locationDao(): LocationDao

    companion object {

        fun buildPersistentCurrency(context: Context):
                RoomData = Room.databaseBuilder(context.applicationContext,
                RoomData::class.java, RoomContract.appdb).build()

    }

}
