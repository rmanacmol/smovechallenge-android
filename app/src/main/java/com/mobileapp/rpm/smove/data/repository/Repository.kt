package com.mobileapp.rpm.smove.data.repository

import android.arch.lifecycle.MutableLiveData
import com.mobileapp.rpm.smove.data.local.RoomData
import com.mobileapp.rpm.smove.data.local.table.LocationEntity
import com.mobileapp.rpm.smove.data.remote.RemoteData
import com.mobileapp.rpm.smove.model.Car
import com.mobileapp.rpm.smove.model.Location
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository
@Inject constructor(
        private val roomData: RoomData,
        private val remoteData: RemoteData
) : IRepository {

    //REMOTE CALL
    val allCompositeDisposable: MutableList<Disposable> = arrayListOf()

    override fun getCar(): MutableLiveData<Car> {
        val mutableLiveData = MutableLiveData<Car>()
        val disposable = remoteData.netCallGetCar()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ car ->
                    mutableLiveData.value = car
                }, { error ->
                    Timber.i("error: " + error.message)
                    mutableLiveData.value = null
                })
        allCompositeDisposable.add(disposable)
        return mutableLiveData
    }

    override fun addLocationToLocal(listUser: List<Location>) {
        val locationList = ArrayList<LocationEntity>()
        listUser.forEach {
            locationList.add(LocationEntity(it.id,
                    it.latitude,
                    it.is_on_trip,
                    it.id,
                    it.longitude
            ))
        }
        roomData.locationDao().insertAll(locationList)
    }

    override fun getLocationLocal(): MutableLiveData<List<Location>> {
        val mutableLiveData = MutableLiveData<List<Location>>()
        val disposable = roomData.locationDao().selectAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ locationList ->
                    mutableLiveData.value = transform(locationList)
                }, { t: Throwable? -> t?.printStackTrace() })
        allCompositeDisposable.add(disposable)
        return mutableLiveData
    }

    private fun transform(users: List<LocationEntity>): ArrayList<Location> {
        val locationList = ArrayList<Location>()
        users.forEach {
            locationList.add(
                    Location(it.latitude,
                            it.is_on_trip,
                            it.id,
                            it.longitude
                    ))
        }
        return locationList
    }
}