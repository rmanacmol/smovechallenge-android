package com.mobileapp.rpm.smove.data.remote

import com.mobileapp.rpm.smove.model.Car
import io.reactivex.Observable
import retrofit2.http.GET

interface RemoteService {

    @GET(RemoteContract.API_GET_LOCATION)
    fun netCallGetCar(): Observable<Car>

}